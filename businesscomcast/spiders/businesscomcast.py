from __future__ import division, absolute_import, unicode_literals
from scrapy import Request, Spider, FormRequest
import re
import json
import traceback
from urllib import urlencode


class BusinessComcastSpider(Spider):
    name = "businesscomcast"
    start_urls = [
        'https://business.comcast.com'
    ]

    BILL_URL = 'https://business.comcast.com/myaccount/api/Billing/GetBillDetails'
    SWITCH_ACCOUNT_URL = 'https://business.comcast.com/myaccount/api/account/switchaccount'
    DOWNLOAD_URL = 'https://business.comcast.com/myaccount/api/Billing/GetSMBStatement'
    passed_accounts = []

    def __init__(self, user_name=None, password=None, *args, **kwargs):
        super(BusinessComcastSpider, self).__init__(*args, **kwargs)
        self.user_name = user_name
        self.password = password
        with open('scrapy.log', 'r') as f:
            self.logs = [i.strip() for i in f.readlines()]
            f.close()

    def parse(self, response):
        return Request(
            'https://business.comcast.com/myaccount/',
            callback=self.login,
            dont_filter=True
        )

    def login(self, response):
        return FormRequest.from_response(response, formid='sign-in-form', formdata={
            'user': self.user_name,
            'passwd': self.password
        }, callback=self.after_login)

    def after_login(self, response):
        return Request(
            'https://business.comcast.com/myaccount/Secure/MyAccount/Bills/BillLanding/',
            callback=self.bill_page,
            dont_filter=True,
            meta=response.meta
        )

    def bill_page(self, response):
        accounts = response.meta.get('accounts')
        if not accounts:
            prefix_api = re.search(r"customerContextApiPublicUri: '(.*?)'", response.body)
            prefix_api = prefix_api.group(1) if prefix_api else None
            user_id = re.search(r"userContextId : '(.*?)'", response.body)
            user_id = user_id.group(1) if user_id else None
            link = '{prefix_api}UserContext/{user_id}/Accounts'.format(prefix_api=prefix_api, user_id=user_id)
            token = re.search(r"token: '(.*?)'", response.body)
            token = token.group(1) if token else None
            if all([
                prefix_api,
                user_id,
                token
            ]):
                return Request(
                    link,
                    callback=self.get_accounts,
                    dont_filter=True,
                    headers={
                        'Accept': 'application/json, text/plain, */*',
                        'Authorization': 'Bearer {}'.format(token),
                    }
                )
        else:
            for key in accounts:
                if key not in self.passed_accounts:
                    return Request(
                        url=self.SWITCH_ACCOUNT_URL,
                        method='POST',
                        dont_filter=True,
                        body=json.dumps({"AccountNumber": key, "PageName": "", "PhoneNumber": ""}),
                        callback=self.switch_accounts,
                        meta=response.meta,
                        headers={
                            'content-type': 'application/json',
                            'accept': 'application/json, text/plain, */*'
                        }
                    )

    def get_accounts(self, response):
        try:
            data = json.loads(response.body)
            accounts = data.get('data', {}).get('accounts', [])
            accounts_dict = {}
            cur_account_id = ''
            for account in accounts:
                account_id = account.get('accountId')
                accounts_dict[account_id] = account
                if account.get('isCurrentAccount'):
                    self.passed_accounts.append(account_id)
                    cur_account_id = account_id
        except:
            self.logger.error('Error in parsing accounts json: {}'.format(traceback.format_exc()))
        else:
            return Request(
                url=self.BILL_URL,
                method='POST',
                callback=self.bill_detail,
                dont_filter=True,
                meta={
                    'accounts': accounts_dict,
                    'cur_account_id': cur_account_id
                }
            )

    def switch_accounts(self, response):
        response.meta['accounts'] = None
        return self.after_login(response)

    def bill_detail(self, response):
        meta = response.meta.copy()
        try:
            data = json.loads(response.body)
            bill_id = data.get('BillSummary', {}).get('BillStatementId')
            bill_start = data.get('BillSummary', {}).get('BillStartDate')
            bill_end = data.get('BillSummary', {}).get('BillEndDate')
            meta['bill_start'] = bill_start
            meta['bill_end'] = bill_end
            meta['bill_id'] = bill_id
        except:
            self.logger.error('Error in parsing the json of bill detail: {}'.format(traceback.format_exc()))
        else:
            if bill_id in self.logs:
                return self.after_login(response)
            return Request(
                url=self.DOWNLOAD_URL,
                method='POST',
                body=urlencode({
                    'BillId': bill_id
                }),
                headers={
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'content-type': 'application/x-www-form-urlencoded'
                },
                callback=self.download_page,
                meta=meta,
                dont_filter=True
            )

    def download_page(self, response):
        meta=response.meta.copy()
        accounts = meta.get('accounts', {})
        cur_account_id= meta.get('cur_account_id', '')
        bill_start = self.date_to_string(meta.get('bill_start'))
        bill_end = self.date_to_string(meta.get('bill_end'))
        bill_id = meta.get('bill_id')
        raw_pdf = response.body
        fname = accounts.get(cur_account_id, {}).get('displayTitle', '')
        file_name = '{}{}_({}-{}).pdf'.format(self.settings.get('DOWNLOAD_DIRECTORY'), fname, bill_start, bill_end)

        with open(file_name, 'wb') as f:
            f.write(raw_pdf)
            f.close()
        self.write_logs(bill_id)

        if len(self.passed_accounts) < len(accounts):
            return self.after_login(response)

    def date_to_string(self, d):
        d = d.split('/')
        return ''.join([i.zfill(2) for i in d])

    def write_logs(self, bill_id):
        with open('scrapy.log', 'a') as f:
            f.write(bill_id + '\n')
            f.close()
        self.logs.append(bill_id)
